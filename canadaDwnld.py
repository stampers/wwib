#!/usr/bin/env python
# add keep searching if no data exists
import requests, io
import math, csv
import pandas as pd
from geopy.geocoders import Nominatim
from datetime import timedelta, date
import time
# check if data exists for townCity today
# get temp
# keep in mind while processing that every province is different with regards to data
def dwnldCadData(townCity, provinceCode):
    geoCoderTries = 0
    while geoCoderTries < 4:
        try:
            loc = getLatLong(townCity + " " + provinceCode)
            latitude = float(loc.latitude)
            longitude = float(loc.longitude)
            break
        except:
                geoCoderTries += 1
                print("# of failed geocoder tries"+str(geoCoderTries))
                time.sleep(2)

        if geoCoderTries >= 3:
            #error_handler("Geocoder Failed")
            return "Geocoder Failed"


    print('downloading Canadian data for '+str(latitude) + ", " + str(longitude))
    print()
    climateIDList = getClimateIDList(latitude, longitude)
    period = 'daily'

    i=0
    while i<len(climateIDList)-1:
        # period = monthly or daily
        print(climateIDList[i])
        envCanUrls = genUrls(provinceCode, period, climateIDList[i][0][3])
        print(envCanUrls)

        # create url first
        j = len(envCanUrls)-1
        offset = 0
        while j > -1:
            print("num envCadUrls = "+str(len(envCanUrls)))
            print("j " + str(j))
            r= requests.get(envCanUrls[j][0])
            print(r.status_code)

            if (str(r.status_code) == '200'):
                s = r.content.decode('utf-8', 'ignore') # remove special characters for use in pandas dataframe
                s = s.encode('utf-8')

                with open("./data/envCadData/"+townCity+'_'+envCanUrls[0][1]+"_"+period+".csv", 'ab') as f:
                    f.write(s[offset:])

                print(r.headers['content-type'])
                print(r.encoding)

                if (j != 1):
                    return True

            # Look for the next closest weather station
            offset = 535 # this will probably break at some future time
            j-=1
        i+=1

    return "No valid urls found"

def getLatLong(location):
    # try and wait a few times before returning error
    print('getLatLong')
    geolocator = Nominatim(user_agent="testApp")
    loc = geolocator.geocode(location)
    return loc


# takes location coordinates as input, searches a csv files, and returns a climateID
def getClimateIDList(latitude, longitude):

    latitudeCeiling = latitude
    latitudeFloor = latitude
    eastmostLongitude = longitude # east of UK is positve from 0 to 180
    westmostLongitude = longitude # moving west decreases longitude in NA
    possibleStations = []
    closestStationID = None
    closestStationIndex = 0
    df = pd.read_csv("./data/climate_station_list.csv")

    stopCounter = 0
    while len(possibleStations) == 0 and stopCounter <= 32:
        # widen search area before each search attempt
        # please note that this will fail around poles and the UK because circles
        eastmostLongitude += stopCounter # east of UK is positve from 0 to 180
        westmostLongitude -= stopCounter # moving west decreases longitude in NA
        latitudeCeiling += stopCounter
        latitudeFloor -= stopCounter
        print("start csv parse")

        i = 0
        while (i < df.shape[0] - 1):
            #print(df.iloc[i]['Latitude'])
            #print("longFloor="+str(eastmostLongitude)+", long= "+str(row['Latitude'])+", longCeiling= "+str(westmostLongitude))

            row = df.iloc[i]
            if (westmostLongitude <= float(row['Longitude']) <= eastmostLongitude):
                #print("latFloor="+str(latitudeFloor)+", lat= "+str(row['Latitude'])+", latCeiling= "+str(latitudeCeiling))
                if (latitudeFloor <= float(row['Latitude']) <= latitudeCeiling):
                # station-name, stationLongitude, stationLatitude, station-elev, CID
                    print("oy!")
                    t = row['name'], float(row['Longitude']), float(row['Latitude']), row['Climate ID']
                    print(t)
                    possibleStations.append(t)
                    #print(possibleStations)
            i+=1
        stopCounter += 1

    sortedPossible = sortStationsDistanceFrom(possibleStations, latitude, longitude)
    #cID = possibleStations[closestStationIndex][3]
    #print(cID)
    #print(possibleStations[closestStationIndex][0])
    #return cID
    return sortedPossible


def sortStationsDistanceFrom(possibleStations, latitude, longitude):
    print("sorting stations")
    # stationDist = ([station-name, stationLongitude, stationLatitude, station-elev, CID], dist)
    stationDist = []

    for station in possibleStations:
        statLon = station[1]
        statLat = station[2]
        dist = math.sqrt((longitude - statLon)**2 + (latitude - statLat)**2)
        stationDist.append((station, dist))

    stationDist.sort(key=lambda x: x[1])

    return stationDist


def genUrls(provCode, period, climateID):
    base_url = "https://dd.weather.gc.ca/climate/observations/"

    daysBack = 30
    dateStart = date.today()+timedelta(days=-daysBack)
    dateEnd = date.today()

    # if days back goes into previous year
    startYear = dateStart.year
    startMonth = dateStart.month
    endMonth = dateEnd.month
    endYear = dateEnd.year

    urls = []

    # if days back goes into previous month
    yyyyE = "_" + str(endYear)
    yyyyS = "_" + str(startYear)

    if endMonth < 10:
        mmE = "-0" +str(endMonth)
    else:
        mmE = "-" + str(endMonth) # valid month range from 01 to 12

    if startMonth < 10:
        mmS = "-0" +str(startMonth)
    else:
        mmS = "-" + str(startMonth) # valid month range from 01 to 12

    # Simplify the statements below
    if period == 'monthly':
        url = base_url + "monthly/csv/"+ provCode +"/climate_monthly_" + provCode + "_" + climateID + yyyyE + "_P1M.csv"
        #t = url, str(endYear)
        t = url, str(endYear)+'-'+str(endMonth)+'-'+str(dateEnd.day)
        urls.append(t)

        if endMonth != startMonth or endYear != startYear:
            url = base_url + "monthly/csv/"+ provCode +"/climate_monthly_" + provCode + "_" + climateID + yyyyS + "_P1M.csv"
            #t = url, str(startYear)
            t = url, str(endYear)+'-'+str(endMonth)+'-'+str(dateEnd.day)
            urls.append(t)

    elif period == 'daily':
        url = base_url + "daily/csv/"+ provCode +"/climate_daily_" + provCode + "_" + climateID + yyyyE + mmE + "_P1D.csv"
        #t = url, str(endYear), str(endMonth)
        t = url, str(endYear)+'-'+str(endMonth)+'-'+str(dateEnd.day)
        urls.append(t)

        if endMonth != startMonth or endYear != startYear:
            url = base_url + "daily/csv/"+ provCode +"/climate_daily_" + provCode + "_" + climateID + yyyyS + mmS + "_P1D.csv"
            # old way
            #t = url, str(startYear), str(startMonth)
            t = url, str(endYear)+'-'+str(endMonth)+'-'+str(dateEnd.day)
            urls.append(t)
    return urls

#getClimateID()
#dwnldCadData('Airdrie', 'AB')

# Example URLS:
 #url = https://climate.weather.gc.ca/climate_data/daily_data_e.html?StationID='+stationID+'&timeframe=2&StartYear='+startYear+'&EndYear='+finYear+'&Day='+day+'&Year='+year+'&Month='+month
    #url = 'https://climate.weather.gc.ca/climate_data/bulk_data_e.html?format=xml&stationID=50430&Year=2020&Month=3&Day=1&timeframe=2&submit=Download+Data'
    #url = 'https://climate.weather.gc.ca/climate_data/bulk_data_e.html?format=csv&stationID=50430&Year=2020&Month=3&Day=1&timeframe=2&submit=Download+Data'
# example urls
#https://dd.weather.gc.ca/climate/observations/daily/csv/AB/
#https://dd.weather.gc.ca/climate/observations/monthly/csv/
#https://dd.weather.gc.ca/climate/observations/normals/csv/1981-2010/AB/
