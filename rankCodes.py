#!/usr/bin/env python
import math, random
import pandas as pd
from datetime import timedelta, date
import os
# check if data exists for townCity today
# get temp
def genNewID(temp_tuple, townCity):
    comparID = ""

    i = 0
    while i < 5:
        r_int = str(random.randint(0,9))
        comparID += r_int
        i+=1
    storeResults(temp_tuple, comparID, townCity)
   
    return comparID

def storeResults(temp_tuple, comparID, townCity):
    s = ""
    fName = './data/index/'+str(comparID)+'.csv'

    #add date
    i=0
    while i < len(temp_tuple):
        s += str(temp_tuple[i])
        if i != len(temp_tuple)-1:
            s += ','
        i+=1
    s += ","+townCity+"\n"

    if os.path.isfile(fName) == False:
        firstRow = 'prevMthAveTemp,curTemp,score,dateRec,townCity\n'
        print('creating new Competition record')
        with open(fName, 'ab') as f:
            f.write(firstRow.encode('utf-8'))
            f.write(s.encode('utf-8'))
            # if file doesn't exist create a initial row with labels
    else:
        with open(fName, 'ab') as f:
            f.write(s.encode('utf-8'))


# normalTemp, currentTemp, diff temp, dateRecorded
def getSortedResults(comparID):
    sortedByScore = []
    df = pd.read_csv('./data/index/'+str(comparID)+'.csv')
    print(df)
    df.sort_values(by=['score'], inplace=True)
    print()
    print(df)
    i = 1
    while (i < df.shape[0] - 1):
        row = df.loc[i]
        sortedByScore.append(row)
        i+=1
    #return sortedByScore
    return df

# normalTemp, currentTemp, diff temp, dateRecorded
def compareResults(resultTuple, comparID):
    comparArry = getSortedResults(comparID)
    return comparArry

#compareResults((1, 2, 1, date.today()), 88075)
