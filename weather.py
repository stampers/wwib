#!/usr/bin/python
import logging, requests, json, time, datetime

from hide import WEATHER_TOKEN
from datetime import timedelta, date
from canadaWeather import getTypicalTemps
from canadaDwnld import dwnldCadData
import numpy as np
import pandas as pd
from datetime import date#, timedelta
import os.path

def getWeather(townCity, provCode, countryCode):
    if countryCode == 'ca':
        cadN = getCanadianNorm(townCity, provCode)
        if isinstance(cadN, str):
            # Assume error
            return cadN
        else:
            cadN = round(cadN, 2)

    elif countryCode == 'us':
        print("USA USA USA")
        # US
    elif countryCode == 'BY':
        return "You won for sure... the weather is perfect. nothing to see here."
    else:
        return "move somewhere else and try again."

    curCad = getCurrentWeather(townCity, provCode)
    p_diff = round(((curCad - cadN)/cadN), 2)
    rating = (curCad-cadN)*p_diff
    temp_data = (cadN, curCad, rating, date.today())
    return temp_data


def getCanadianNorm(townCity, provCode):
    daysBack = 30
    dateStart = date.today()+timedelta(days=-daysBack)
    dateEnd = date.today()

    yyyy = str(dateEnd.year)
    m = str(dateEnd.month)
    d = str(dateEnd.day)
    # figure out how to deal with multiple years/ months
   
    path = './data/envCadData/'
    fName = path + townCity+'_'+yyyy+'-'+m+'-'+d+'_daily.csv'
    #df = pd.read_csv(fName)

    if os.path.isfile(fName) == False:
        print('dwnlding cad data')
        test = dwnldCadData(townCity, provCode)
        if test != True:
            return "Download Error "+str(test)

    # add error checking here
    return getTypicalTemps(fName, dateStart, dateEnd)
    print("Your City "+townCity)


def getCurrentWeather(townCity, provCode):
    city = townCity
    dataResponse = requests.get("http://api.openweathermap.org/data/2.5/weather?q="+city+"&appid="+WEATHER_TOKEN)
    #weatherDict = json.loads(str(weatherObj)

    try:
        weatherObj = dataResponse.json()

    except (ValueError, KeyError, TypeError):
        print("Json formating error")

    tempC1 = round(weatherObj['main']['temp'] - 273.15, 2)
    print(dataResponse)
    return tempC1
