# Whos weather feels better?

If you live somewhere colder than where your friend lives, but you still want to brag about the fact that your weather feels warmer than theirs because it is warmer than normal for you.


This project is written in python.
It relies on the telegram bot api and open-weather api.
The open-weather api dependency is going to be removed shortly in favour of environment canada and noaa datasources.

## Installation
setup a python virtual environment
pip install requirements.txt
Add openWeather and telegram bot tokens to hide.py
## To run
python fsm.py

This bot is sometimes running in telegram @wwib_bot