#!/usr/bin/env python

import csv, datetime
from datetime import timedelta, date
import array as arr
import pandas as pd
# Date/Time
# Mean Temp (°C)
# Mean Temp Flag
#
def getTypicalTemps(fName, dateStart, dateEnd):
    df = pd.read_csv(fName)
    i = 0
    temps = []
    while (i < df.shape[0] - 1):
        #print(df.iloc[i]['Latitude'])
        row = df.iloc[i]
        rowDate = datetime.date(int(row['Year']), int(row['Month']), int(row['Day']))
        if rowDate >= dateStart and str(row['Mean Temp (C)']) != 'nan':
            print(str(row['Mean Temp (C)']))
            temps.append(float(row['Mean Temp (C)']))
        #print("longFloor="+str(eastmostLongitude)+", long= "+str(row['Latitude'])+", longCeiling= "+str(westmostLongitude))

        i+=1
    print(temps)
    return sum(temps)/len(temps)


def getTypicalTempsOldMethod(location):
    daysBack = 30
    dateStart = str(date.today()+timedelta(days=-daysBack))
    dateEnd = str(date.today()+timedelta(days=-1))
    dailyTemps = []

    with open(location+'.csv') as data_file:
        csv_reader = csv.reader(data_file, delimiter=',')
        line_count = 0

        started = False
        dailyTemps.append(1.1)
        for row in csv_reader:

            if row[4] == dateStart:
                started = True
                #print(row[4]+', '+row[13])
                if (row[14] != 'M'):
                    dailyTemps.append(float(row[13]))

            elif started:
                #print(row[4]+', '+row[13])
                if (row[14] != 'M'):
                    dailyTemps.append(float(row[13]))

                if row[4] == dateEnd:
                      break

            line_count += 1

        print(f'Processed {line_count} lines.')
        print(dailyTemps)
    return dailyTemps

def testDates():
    #d = datetime.date(time.time())
    d = date.today()
    t = timedelta(days=-30)
    t = d + t
    print(d)
    print(t)

#testDates()
#getTypicalTemps('calgary1')
